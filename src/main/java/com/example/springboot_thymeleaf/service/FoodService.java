package com.example.springboot_thymeleaf.service;

import com.example.springboot_thymeleaf.Repository.FoodRepository;
import com.example.springboot_thymeleaf.model.Food;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class FoodService {

    FoodRepository foodMapper;

    public FoodService(FoodRepository foodMapper) {
        this.foodMapper = foodMapper;
    }

    public Food[] getFoods() {
        return foodMapper.searchBy();
    }

    public Food addFood(Food food) {
        return foodMapper.save(food);
    }
}

