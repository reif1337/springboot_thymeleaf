package com.example.springboot_thymeleaf.Repository;

import com.example.springboot_thymeleaf.model.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodRepository extends JpaRepository<Food, Long> {
    Food[] searchBy();

}
