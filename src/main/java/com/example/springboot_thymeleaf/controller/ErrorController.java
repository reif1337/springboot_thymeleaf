package com.example.springboot_thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

//Not yet ready

@Controller
@RequestMapping("/error")
public class ErrorController {

    @GetMapping
    public String handleError(Model model) {
        // Retrieve the error details from the request
        Object status = model.getAttribute("status");
        Object error = model.getAttribute("error");
        Object exception = model.getAttribute("exception");

        // Handle the error and prepare the response
        String errorMessage = "An error occurred";
        if (status != null) {
            errorMessage = String.format("Error code: %s", status.toString());
        }
        if (error != null) {
            errorMessage = String.format("%s: %s", errorMessage, error.toString());
        }
        if (exception != null) {
            errorMessage = String.format("%s: %s", errorMessage, exception.toString());
        }

        // Add the error message to the model for the error page
        model.addAttribute("errorMessage", errorMessage);

        // Return the error page template
        return "error";
    }
}