package com.example.springboot_thymeleaf.controller;

import com.example.springboot_thymeleaf.model.Food;
import com.example.springboot_thymeleaf.service.FoodService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FoodController {

    private final FoodService foodService;

    public FoodController(FoodService foodService) {
        this.foodService = foodService;
    }

    @GetMapping("/home")
    //Muss sein
    public String getHome(Model model, Food food) {
        model.addAttribute("message", "Hello, I am the FoodController!");

        model.addAttribute("foods", foodService.getFoods());


        return "home";
    }


    @PostMapping("/home")
    public String addFood(Model model, Food food) {

        if (food.getName() == null || food.getName().isEmpty()) {
            model.addAttribute("errorMessage", String.format("Could not add %s needs a name", food.getName()));
        }
        else if (ValidationUtils.validateName(food.getName())) {
            model.addAttribute("errorMessage", String.format("Could not add %s name too short", food.getName()));
        }
        else if (food.getPrice() == null) {
            model.addAttribute("errorMessage", String.format("Could not add %s missing price", food.getName()));
        }
        else if (food.getPrice() > 1000) {
            model.addAttribute("errorMessage", String.format("Could not add %s it is too expansive", food.getName()));
        } else if (food.getPrice() < 1) {
            model.addAttribute("errorMessage", String.format("Could not add %s it is too cheep", food.getName()));
        } else if (foodService.addFood(food) == null) {
            model.addAttribute("errorMessage", String.format("Could not add %s ", food.getName()));
        } else {
            model.addAttribute("successMessage", String.format("Successfully added %s", food.getName()));
        }

        model.addAttribute("foods", foodService.getFoods());
        return "home";
    }
}


class ValidationUtils {
    public static boolean validateName(String name) {
        return name.length() < 3;
    }
}