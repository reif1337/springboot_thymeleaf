CREATE TABLE IF NOT EXISTS FOOD (
                      id Long NOT NULL AUTO_INCREMENT,
                      name VARCHAR(255) NOT NULL,
                      price DOUBLE NOT NULL,
                      PRIMARY KEY (id)
);